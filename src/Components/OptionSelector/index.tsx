import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './style'

export const OptionSelector = ({ txtLeftBtn, txtRightBtn, leftPressed = true, onPressSelector }) => {
    const { styBox, styBtn, btnLeft, btnRight, styTextBase, styTxt, textPress, selectorPress } = styles
    return (
        <View style={styBox}>
            <TouchableOpacity
                activeOpacity={1}
                onPress={onPressSelector}
                style={leftPressed ? [styBtn, btnLeft, selectorPress] : [styBtn, btnLeft]}>
                <Text ellipsizeMode='tail' numberOfLines={1}
                    style={leftPressed ? [styTextBase, textPress] : [styTextBase, styTxt]}>
                    {txtLeftBtn.toUpperCase()}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                activeOpacity={1}
                onPress={onPressSelector}
                style={!leftPressed ? [styBtn, btnRight, selectorPress] : [styBtn, btnRight]}>
                <Text ellipsizeMode='tail' numberOfLines={1}
                    style={!leftPressed ? [styTextBase, textPress] : [styTextBase, styTxt]}>
                    {txtRightBtn.toUpperCase()}
                </Text>
            </TouchableOpacity>
        </View>
    )
}