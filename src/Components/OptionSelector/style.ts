import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    styBtn: {
        width: 170,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderWidth: 0.5,
        overflow: 'hidden',
        borderColor: 'rgba(234, 100, 34, 1.0)',
        alignItems: 'center'
    },
    btnLeft: {
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3
    },
    selectorPress: {
        backgroundColor: 'rgba(234, 100, 34, 1.0)'
    },
    btnRight: {
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3
    },
    styTextBase: {
        maxWidth: 160,
        letterSpacing: 0,
        fontSize: 13,
        lineHeight: 13
    },
    styTxt: {
        color: 'rgba(100, 100, 100, 1.0)'
    },
    textPress: {
        color: 'rgba(250, 250, 250, 1.0)'
    }
})