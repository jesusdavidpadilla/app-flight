import React from 'react'
import { View } from 'react-native'
import { ToolbarInfo } from '../../Components/ToolbarInfo'
import { CardFlights } from '../../Components/CardFlights'
import { BootbarPrice } from '../../Components/BootbarPrice'

export const RoundTrip = ({ infoFlight: { flightFrom, flightTo } }) => {
    const { destination: destFrom, origin: oriFrom,
        price: { value: valueFrom },
        airline: airFrom, detail: detaFrom, segments: segmFrom } = flightFrom

    const { destination: destTo, origin: oriTo,
        price: { currency: curreTo, value: valueTo },
        airline: airTo, detail: detaTo, segments: segmTo } = flightTo

    return (
        <View style={{ marginVertical: 5 }}>
            <ToolbarInfo flightTo={true} description={oriFrom.date} />
            <CardFlights origin={oriFrom} destination={destFrom}
                aeroline={airFrom} detail={detaFrom} segments={segmFrom} />
            <ToolbarInfo flightTo={false} description={oriTo.date} />
            <CardFlights origin={oriTo} destination={destTo}
                aeroline={airTo} detail={detaTo} segments={segmTo} />
            <BootbarPrice currency={curreTo} price={valueFrom + valueTo} />
        </View>
    )
}