import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './style'

export interface ICityFlight {
    icon?: string,
    cityshort: string,
    hourFlight: string
}

export const CityFlight = ({ icon, cityshort, hourFlight }: ICityFlight) => {
    const { styBox, styImg, styText, styCityShort, styHour } = styles
    return (
        <View style={[styBox]}>
            <Image style={styImg} source={{ uri: icon }} />
            <View>
                <Text style={[styText, styCityShort]} ellipsizeMode='tail' numberOfLines={1}>{cityshort}</Text>
                <Text style={[styText, styHour]} ellipsizeMode='tail' numberOfLines={1}>{hourFlight}</Text>
            </View>
        </View>

    )
}