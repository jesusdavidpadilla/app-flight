import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        width: 120,
        paddingHorizontal: 10,
        paddingVertical: 5,
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    styImg: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginRight: 5
    },
    styText: {
        maxWidth: 100,
        letterSpacing: 0
    },
    styCityShort: {
        fontSize: 18,
        lineHeight: 18,
        fontWeight: '600',
        color: 'rgba(0, 0, 0, 1.0)'
    },
    styHour: {
        fontSize: 11,
        lineHeight: 11,
        color: 'rgba(100, 100, 100, 1.0)'
    }
})