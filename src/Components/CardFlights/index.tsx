import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import {CityFlight} from '../CityFlight'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { Actions } from 'react-native-router-flux'
import styles from './style'

export const CardFlights = ({ origin, destination, aeroline, detail, segments}) => {
    const { styBox, styBoxIcons, styBoxAeroline, styTxt, styLogo, styIconInfo, styCity } = styles

    return (
        <View style={styBox}>
            <View style={styBoxIcons}>
                <View style={styBoxAeroline}>
                    <Image style={styLogo} source={{ uri: aeroline.img }}/>
                    <Text style={styTxt}>{aeroline.name}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => { Actions.detailFlight({detail}) }}>
                    <Icon style={styIconInfo} name={'info-outline'} />
                </TouchableOpacity>
            </View>
            <View style={styCity}>
                <CityFlight icon={origin.img} cityshort={origin.iata} hourFlight={origin.hour}/>
                <Icon style={styIconInfo} name={'trending-flat'} />
                <CityFlight icon={destination.img} cityshort={destination.iata} hourFlight={destination.hour}/>
            </View>
        </View>
    )
}