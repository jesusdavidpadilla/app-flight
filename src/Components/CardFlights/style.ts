import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        width: 340,
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
        paddingVertical: 10
    },
    styBoxIcons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    styBoxAeroline: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    styLogo: {
        width: 20,
        height: 20,
        borderRadius: 10
    },
    styTxt: {
        fontSize: 13,
        letterSpacing: 0,
        lineHeight: 13,
        color: 'rgba(100, 100, 100, 1.0)'
    },
    styIconInfo: {
        fontSize: 25,
        justifyContent: 'center',
        color: 'rgba(23, 158, 167, 1.0)',
        marginHorizontal: 5
    },
    styCity: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})