import React from 'react'
import { View, ScrollView, FlatList } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { FormButton } from '../FormButton'
import { RecordData } from '../RecordData'
import * as Animatable from 'react-native-animatable'
import styles from './style'

export const DetailFlight = ({detail}) => {
    let animateFadeOut: any
    const { styScreenModal, styBoxDetail, styText } = styles
    return (
        <View style={styScreenModal}>
        <Animatable.View
            ref = {ref => animateFadeOut = ref}>
            <View style={styBoxDetail}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <FlatList
                        data={detail}
                        renderItem={({ item }) => <RecordData data={item} key={item._id} />}
                        keyExtractor={(item, index) => index.toString()} />
                    <FormButton
                        textBtn={'Cerrar'}
                        onPressBtnForm={() => {
                            animateFadeOut.fadeOut(200)
                            .then(() => Actions.pop())}}/>
                </ScrollView>
            </View>
            </Animatable.View>
        </View>
    )
}