import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styScreenModal: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    styBoxDetail: {
        width: 300,
        maxHeight: 400,
        padding: 10,
        borderRadius: 3,
        backgroundColor: 'rgba(255,255,255,1.0)'
    }
})