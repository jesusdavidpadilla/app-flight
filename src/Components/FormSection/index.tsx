import React, { Component } from 'react'
import { View } from 'react-native'
import PickerField from '../PickerField'
import DateInput from '../DateInput'
import styles from './style'

export default class FormSection extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            cityFrom: null,
            cityTo: null,
            dateFrom: new Date()
        }
    }

    onChangeCityFrom(cityFrom) {
        this.setState({ cityFrom: cityFrom })
        this.props.changeCityFrom(cityFrom)
    }

    onChangeCityTo(cityTo) {
        this.setState({ cityTo: cityTo })
        this.props.changeCityTo(cityTo)
    }

    onChangeDate(date) {
        this.setState({ dateFrom: date })
        this.props.changeDate(date)
    }

    render() {
        const { styBox, styTxt } = styles
        const { cities } = this.props
        const { cityFrom, cityTo, dateFrom } = this.state
        return (
            <View style={styBox}>
                <PickerField
                    label={'Desde'}
                    records={cities}
                    displayName={'nameLarge'}
                    onChange={(itemSelect) => { this.onChangeCityFrom(itemSelect) }}
                />
                <PickerField
                    label={'Hasta'}
                    records={cities}
                    displayName={'nameLarge'}
                    onChange={(itemSelect) => { this.onChangeCityTo(itemSelect) }}
                />
                <DateInput
                    textLabel={'Salgo el'}
                    dateValue={dateFrom}
                    minDate={new Date()}
                    onchangeDate={(date) => { this.onChangeDate(date) }} />
            </View>
        )
    }
}