import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        width: 340,
        paddingVertical: 10
    },
    picker: {
        flex: 1,
        color: '#6D6D6D',
        backgroundColor: '#FFF',
        marginBottom: 20,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    }
})