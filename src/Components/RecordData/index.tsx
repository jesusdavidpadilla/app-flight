import React from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './style'

export interface IRecordData {
    icon?: string,
    tittle: string,
    description?: string
}

export const RecordData = ( {data: {icon, tittle, description}} ) => {
// export const RecordData = ({ icon, tittle, description }: IRecordData) => {
    const { styBox, styIcon, styBoxText, styText, styTittle, styDescription } = styles
    return (
        <View style={styBox}>
            {icon && <Icon style={styIcon} name={icon}/>}
            <View style={styBoxText}>
                <Text style={[styText, styTittle]} ellipsizeMode='tail' numberOfLines={1}>{tittle}</Text>
                <Text style={[styText, styDescription]} ellipsizeMode='tail' numberOfLines={1}>{description}</Text>
            </View>
        </View>
    )
}