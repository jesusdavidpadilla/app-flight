import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: 350,
        height: 40,
        paddingHorizontal: 5,
        marginVertical: 3,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        borderBottomWidth: 0.1,
        borderBottomColor: 'rgba(220, 220, 220, 0.5)'
    },
    styIcon: {
        fontSize: 20,
        justifyContent: 'center',
        color: 'rgba(100, 100, 100, 1.0)',
        margin: 5
    },
    styBoxText: {
        alignItems: 'flex-start'
    },
    styText: {
        maxWidth: 300,
        paddingHorizontal: 5,
        letterSpacing: 0
    },
    styTittle: {
        fontSize: 13,
        lineHeight: 13,
        fontWeight: '600',
        color: 'rgba(50, 50, 50, 1.0)'
    },
    styDescription: {
        fontSize: 11,
        lineHeight: 11,
        color: 'rgba(100, 100, 100, 1.0)'
    }
})