import React from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './style'

export const BootbarPrice = ({ currency, price }) => {
    const { styBox, styBoxItems, styIcon, styBoxText, styText, styTittle, styDescription, styPrice } = styles
    return (
        <View style={styBox}>
            <View style={styBoxItems}>
                <Icon style={styIcon} name={'credit-card'} />
                <View style={styBoxText}>
                    <Text style={[styText, styTittle]}>{'Precio Total'}</Text>
                    <Text style={[styText, styDescription]}>{'Impuestos incluidos'}</Text>
                </View>
            </View>
            <Text style={[styText, styPrice]} ellipsizeMode='tail' numberOfLines={1}>{`${currency} ${price}`}</Text>
        </View>
    )
}