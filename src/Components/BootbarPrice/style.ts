import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 340,
        paddingHorizontal: 5,
        backgroundColor: 'rgba(14, 49, 66, 1.0)'
    },
    styBoxItems: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    styIcon: {
        fontSize: 15,
        justifyContent: 'center',
        color: 'rgba(255, 255, 255, 1.0)',
        margin: 5
    },
    styBoxText: {
        marginVertical: 5,
        alignItems: 'flex-start'
    },
    styText: {
        maxWidth: 115,
        paddingHorizontal: 5,
        letterSpacing: 0
    },
    styTittle: {
        fontSize: 12,
        lineHeight: 12,
        fontWeight: '600',
        color: 'rgba(255, 255, 255, 1.0)'
    },
    styDescription: {
        fontSize: 10,
        lineHeight: 10,
        color: 'rgba(255, 255, 255, 1.0)'
    },
    styPrice: {
        fontSize: 14,
        lineHeight: 14,
        fontWeight: '600',
        color: 'rgba(255, 255, 255, 1.0)'
    }
})