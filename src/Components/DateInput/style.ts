import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 8,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 3,
        overflow: 'hidden',
        backgroundColor: 'rgba(255, 255, 255, 1.0)'
    },
    styBoxText: {
        alignItems: 'flex-start'
    },
    styLabel: {
        fontSize: 12,
        lineHeight: 12,
        paddingVertical: 2,
        letterSpacing: 0,
        fontWeight: '600',
        color: 'rgba(234, 100, 34, 1.0)'
    },
    styText: {
        fontSize: 14,
        lineHeight: 14,
        paddingVertical: 2,
        letterSpacing: 0,
        color: 'rgba(100, 100, 100, 1.0)'
    },
    styIconCalendar: {
        fontSize: 20,
        justifyContent: 'center',
        color: 'rgba(100, 100, 100, 1.0)',
        margin: 5
    }
})