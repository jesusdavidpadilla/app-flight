import React, {Component} from 'react'
import { View, Text, TouchableOpacity, DatePickerAndroid, Platform } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Sugar from 'sugar'
import styles from './style'

export default class DateInput extends Component<{Props}> {
    constructor(props) {
        super(props)
        this.state = {
            dateValueLocal: props.dateValue
        }
    }

    showAndroidDatePicker = async () => {
        const { onchangeDate, minDate } = this.props
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: new Date(this.state.dateValueLocal),
                minDate: minDate
            })
            if (action !== DatePickerAndroid.dismissedAction) {
                const date = new Date(year, month, day)
                this.setState({ dateValueLocal: date})
                onchangeDate(date)
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message)
        }
    }

    render() {
        const { styBox, styBoxText, styLabel, styText, styIconCalendar } = styles
        return (
            <TouchableOpacity style={styBox}
                activeOpacity={1}
                onPress={() => {
                   if (Platform.OS !== 'ios') {
                       this.showAndroidDatePicker()
                    }
                }}>
                <View style={styBoxText}>
                    <Text style={styLabel}>{this.props.textLabel}</Text>
                    <Text style={styText}>
                        {Sugar.Date.format(this.state.dateValueLocal, '{year}-{MM}-{dd}')}
                    </Text>
                </View>
                <Icon style={styIconCalendar} name={'event'} />
            </TouchableOpacity>
        )
    }
}