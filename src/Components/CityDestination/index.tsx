import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PickerField from '../PickerField'
import styles from './style'

export default class CityDestination extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            currentCity: null
        }
    }

    onChangeCity(value) {
        this.setState({currentCity: value})
        this.props.changeCity(value)
    }

    render() {
        const { styBox, styText, styCityDesc, styBoxPicker, styPicker } = styles
        const { tittle, options } = this.props
        const { currentCity } = this.state
        return (
            <View style={styBox}>
                <PickerField
                    label={tittle}
                    records={options}
                    displayName={'aeroName'}
                    onChange={(value) => { this.onChangeCity(value) }}
                    styBoxOpt={styBoxPicker}
                    styPickerOpt={styPicker}
                />
                <Text style={[styText, styCityDesc]} ellipsizeMode='tail' numberOfLines={1}>
                    {currentCity ? currentCity.nameLarge : ''}
                </Text>
            </View>

        )
    }
}