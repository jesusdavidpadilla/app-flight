import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        width: 140,
        marginVertical: 10,
        padding: 10,
        borderRadius: 3,
        backgroundColor: 'rgba(255, 255, 255, 1.0)',
        overflow: 'hidden'
    },
    styText: {
        maxWidth: 100,
        paddingVertical: 2,
        letterSpacing: 0
    },
    styCityDesc: {
        fontSize: 11,
        lineHeight: 11,
        color: 'rgba(100, 100, 100, 1.0)'
    },
    styBoxPicker: {
        marginTop: 0,
        paddingHorizontal: 0,
        paddingVertical: 0
    },
    styPicker: {
        width: 140
    }
})