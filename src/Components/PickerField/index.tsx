import React, { Component } from 'react'
import { View, Text, Picker } from 'react-native'
import styles from './style'

export default class PickerField extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            value: this.props.value
        }
    }

    onChangeValue(itemValue) {
        this.setState({ value: itemValue })
        this.props.onChange(itemValue)
    }

    render() {
        const { styBox, styLabel, styPicker } = styles
        const { label, records, displayName, styBoxOpt, styPickerOpt } = this.props
        return (
            <View style={[styBox, styBoxOpt]}>
                <Text style={styLabel}>{label}</Text>
                <Picker
                    selectedValue={this.state.value}
                    style={[styPicker, styPickerOpt]}
                    mode={'dropdown'}
                    onValueChange={(itemValue) => this.onChangeValue(itemValue)}>
                    {records && records.map(function (item) {
                        return <Picker.Item
                            label={item[displayName]} value={item}
                            key={item[displayName] + item._id} />
                    })}
                </Picker>
            </View>
        )
    }
}