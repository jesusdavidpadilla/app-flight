import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
styBox: {
        alignItems: 'flex-start',
        marginTop: 8,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 3,
        overflow: 'hidden',
        backgroundColor: 'rgba(255, 255, 255, 1.0)'
    },
    styLabel: {
        fontSize: 12,
        lineHeight: 12,
        paddingVertical: 2,
        letterSpacing: 0,
        fontWeight: '600',
        color: 'rgba(234, 100, 34, 1.0)'
    },
    styPicker: {
        width: 340,
        height: 20
    }
})