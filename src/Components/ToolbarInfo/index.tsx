import React from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './style'

export const ToolbarInfo = ({ flightTo, description }) => {
    const { styBox, styIcon, styText, styTittle, styDescription } = styles
    return (
        <View style={styBox}>
            <Icon style={styIcon} name={flightTo ? 'flight-takeoff' : 'flight-land'}/>
            <Text style={[styText, styTittle]}>{flightTo ? 'IDA' : 'REGRESO'}</Text>
            <Text style={[styText, styDescription]} ellipsizeMode='tail' numberOfLines={1}>{description}</Text>
        </View>
    )
}