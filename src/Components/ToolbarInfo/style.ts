import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width: 340,
        paddingHorizontal: 5,
        backgroundColor: 'rgba(234, 100, 34, 1.0)'
    },
    styIcon: {
        fontSize: 15,
        justifyContent: 'center',
        color: 'rgba(255, 255, 255, 1.0)',
        margin: 5
    },
    styText: {
        maxWidth: 100,
        paddingVertical: 3,
        paddingHorizontal: 5,
        letterSpacing: 0
    },
    styTittle: {
        fontSize: 13,
        lineHeight: 13,
        color: 'rgba(255, 255, 255, 1.0)'
    },
    styDescription: {
        fontSize: 13,
        lineHeight: 13,
        color: 'rgba(255, 255, 255, 1.0)'
    }
})