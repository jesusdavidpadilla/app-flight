import React from 'react'
import { View } from 'react-native'
import { ToolbarInfo } from '../../Components/ToolbarInfo'
import { CardFlights } from '../../Components/CardFlights'
import { BootbarPrice } from '../../Components/BootbarPrice'

export const OneWay = ({ infoFlight }) => {
    const { destination, origin, price: { currency, value }, airline, detail, segments } = infoFlight
    return (
        <View style={{marginVertical: 5}}>
            <ToolbarInfo flightTo={true} description={origin.date} />
            <CardFlights origin={origin} destination={destination}
                aeroline={airline} detail={detail} segments={segments} />
            <BootbarPrice currency={currency} price={value} />
        </View>
    )
}