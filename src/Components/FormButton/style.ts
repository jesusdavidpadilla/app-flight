import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBox: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 8,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 3,
        borderWidth: 0.5,
        borderColor: 'rgba(234, 100, 34, 0.5)',
        overflow: 'hidden'
    },
    styFillColor: {
        alignItems: 'center',
        position: 'absolute',
        width: 600,
        height: 600,
        opacity: 0,
        backgroundColor: 'rgba(234, 100, 34, 0.2)'
    },
    styCircleAnimate: {
        alignItems: 'center',
        position: 'absolute',
        width: 100,
        height: 100,
        borderRadius: 50,
        opacity: 0,
        backgroundColor: 'rgba(234, 100, 34, 0.5)'
    },
    styText: {
        fontSize: 16,
        lineHeight: 16,
        letterSpacing: 0,
        fontWeight: '600',
        color: 'rgba(234, 100, 34, 1.0)'
    }
})