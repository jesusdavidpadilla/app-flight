import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import styles from './style'
import * as Animatable from 'react-native-animatable'
import { initializeButtonAnimate } from '../../Helpers/Animated'

export interface IFormButton {
    textBtn: string,
    onPressBtnForm: any,
    styBoxOpt?: any,
    styTextOpt?: any
}

export const FormButton = ({ textBtn, onPressBtnForm, styBoxOpt, styTextOpt }: IFormButton) => {
    initializeButtonAnimate()
    let animateFill, animateWave: any
    const refAnimateFill = ref => animateFill = ref
    const refAnimateWave = ref => animateWave = ref
    const { styBox, styFillColor, styText, styCircleAnimate } = styles
    const animateBtn = () => {
        animateFill.fill()
        animateWave.waveOut()
        .then(() => onPressBtnForm())
    }
    return (
        <TouchableOpacity style={[styBox, styBoxOpt]}
            activeOpacity={1}
            onPress={() => animateBtn()}>
            <Animatable.View
                ref={refAnimateFill}
                duration={950}
                style={styFillColor}
            />
            <Animatable.View
                ref={refAnimateWave}
                duration={750}
                style={styCircleAnimate}
            />
            <Text style={[styText, styTextOpt]} ellipsizeMode='tail' numberOfLines={1}>{textBtn.toUpperCase()}</Text>
        </TouchableOpacity>
    )
}