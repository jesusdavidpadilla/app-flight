const url = 'https://flights-xpzvgavtrp.now.sh'

export const getCities = async () => {
    try {
        const response = await fetch(`${url}/cities`)
        const responseJson = await response.json()
        return responseJson.cities
    } catch (error) {
        console.error(error)
    }
}
export const findFlights = async (cityFrom, cityTo, date) => {
    try {
        const response = await fetch(`${url}/find/${cityFrom}/${cityTo}/${date}`)
        const responseJson = await response.json()
        return responseJson.flight
    } catch (error) {
        console.error(error)
    }
}
export const findRoundTrip = async (cityFrom, cityTo, dateFrom, dateReturn) => {
    try {
        const response = await fetch(`${url}/find/${cityFrom}/${cityTo}/${dateFrom}/${dateReturn}`)
        const responseJson = await response.json()
        return responseJson.flight
    } catch (error) {
        console.error(error)
    }
}
export const findMultiDestiny = async (tripData: any) => {
    try {
        let multicity = await fetch(`${url}/find/multiflights`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify(tripData)
        })
        multicity = await multicity.json()
        return multicity
    } catch (error) {
        console.error(error)
    }
}