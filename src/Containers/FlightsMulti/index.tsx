import React, { Component } from 'react'
import { View, FlatList, ActivityIndicator } from 'react-native'
import { OneWay } from '../../Components/OneWay'
import { findFlights, findMultiDestiny } from '../../Services'
import Sugar from 'sugar'
import styles from './style'

export default class FlightsMulti extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }
    componentDidMount() {
            findMultiDestiny(this.props.data)
            .then(flights => {
                this.setState({ data: flights })
            })
    }
    render() {
        const { styBoxFlights } = styles
        const { data } = this.state
        return (
            <View style={styBoxFlights}>
                {data.length === 0 ? <ActivityIndicator size='large' color='rgba(255, 143, 41, 1.0)' /> :
                    <FlatList data={data}
                        renderItem={({ item }) => <OneWay infoFlight={item} key={item._id} />}
                        keyExtractor={(item, index) => index.toString()} />}
            </View>
        )
    }
}