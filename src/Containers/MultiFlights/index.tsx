import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import FormSection from '../../Components/FormSection'
import { FormButton } from '../../Components/FormButton'
import styles from './style'

export default class MultiFlights extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            trip1: {
                cityFrom: null,
                cityTo: null,
                dateFrom: null
            },
            trip2: {
                cityFrom: null,
                cityTo: null,
                dateFrom: null
            },
            trip3: {
                cityFrom: null,
                cityTo: null,
                dateFrom: null
            }
        }
    }

    render() {
        const { styBoxFlights } = styles
        return (
            <View style={styBoxFlights}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text>Viaje 1</Text>
                    <FormSection
                        changeCityFrom={(cityFrom) => {this.setState({trip1: {cityFrom: cityFrom}})}}
                        changeCityTo={(cityTo) => {this.setState({trip1: {cityTo: cityTo}})}}
                        changeDate={(dateFrom) => {this.setState({trip1: {dateFrom: dateFrom}})}}
                        cities={this.props.cities}
                    />
                    <Text>Viaje 2</Text>
                    <FormSection
                        changeCityFrom={(cityFrom) => {this.setState({trip2: {cityFrom: cityFrom}})}}
                        changeCityTo={(cityTo) => {this.setState({trip2: {cityTo: cityTo}})}}
                        changeDate={(dateFrom) => {this.setState({trip2: {dateFrom: dateFrom}})}}
                        cities={this.props.cities}
                    />
                    <Text>Viaje 3</Text>
                    <FormSection
                        changeCityFrom={(cityFrom) => {this.setState({trip3: {cityFrom: cityFrom}})}}
                        changeCityTo={(cityTo) => {this.setState({trip3: {cityTo: cityTo}})}}
                        changeDate={(dateFrom) => {this.setState({trip3: {dateFrom: dateFrom}})}}
                        cities={this.props.cities}
                    />
                    <FormButton
                        textBtn={'Buscar'}
                        onPressBtnForm={() => {}}
                        />
                </ScrollView>
            </View>
        )
    }
}