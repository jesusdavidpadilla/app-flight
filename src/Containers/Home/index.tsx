import React, { Component } from 'react'
import { View, ScrollView, ActivityIndicator } from 'react-native'
import styles from './style'
import { Actions } from 'react-native-router-flux'
import CityDestination from '../../Components/CityDestination'
import { OptionSelector } from '../../Components/OptionSelector'
import { FormButton } from '../../Components/FormButton'
import DateInput from '../../Components/DateInput'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { getCities } from '../../Services'
import { validateForm, showAlert } from '../../Helpers/wrapper'

export default class Home extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            cities: [],
            selectorLeftPress: true,
            dateFrom: new Date(),
            dateTo: new Date(),
            cityFrom: null,
            cityTo: null
        }
    }

    componentDidMount() {
        getCities().then(cities => this.setState({ cities: cities }))
    }

    render() {
        const { styBoxHome, styBoxDestination, styIconDestination, styTextOpt } = styles
        let { cities, selectorLeftPress, dateFrom, dateTo, cityFrom, cityTo } = this.state
        return (
            <View style={styBoxHome}>
                {cities.length === 0 ? <ActivityIndicator size='large' color='rgba(255, 143, 41, 1.0)' /> :
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <OptionSelector
                            txtLeftBtn={'Ida y regreso'}
                            txtRightBtn={'Solo ida'}
                            onPressSelector={() => { this.setState({ selectorLeftPress: !selectorLeftPress }) }}
                            leftPressed={selectorLeftPress}
                        />
                        <FormButton
                            textBtn={'Multidestino'}
                            styTextOpt={styTextOpt}
                            onPressBtnForm={() => { Actions.multiFlights({cities})}} />
                        <View style={styBoxDestination}>
                            <CityDestination
                                tittle={'Desde'} options={this.state.cities}
                                changeCity={(city) => this.setState({ cityFrom: city })} />
                            <Icon style={styIconDestination} name={selectorLeftPress ? 'repeat' : 'call-made'} />
                            <CityDestination
                                tittle={'Hasta'} options={this.state.cities}
                                changeCity={(city) => this.setState({ cityTo: city })} />
                        </View>
                        <DateInput
                            textLabel={'Salgo el'}
                            dateValue={dateFrom}
                            minDate={new Date()}
                            onchangeDate={(date) => { this.setState({ dateFrom: date }) }}
                        />
                        {this.state.selectorLeftPress && <DateInput
                            textLabel={'Regreso el'}
                            dateValue={dateTo}
                            minDate={new Date()}
                            onchangeDate={(date) => { this.setState({ dateTo: date }) }}
                        />}
                        <FormButton
                            textBtn={'Buscar'}
                            onPressBtnForm={() => {
                                if (validateForm([dateFrom, dateTo, cityFrom, cityTo])) {
                                    selectorLeftPress ?
                                    Actions.flightsRoundTrip({ data: { dateFrom, dateTo, cityFrom, cityTo } })
                                    : Actions.flightsOneWay({ data: { dateFrom, cityFrom, cityTo } })
                                } else {
                                    showAlert('Campos invalidos',
                                        'Ingrese los campos de inicio y destino del vuelo al igual que sus fechas')
                                }
                            }} />
                    </ScrollView>
                }
            </View>
        )
    }
}