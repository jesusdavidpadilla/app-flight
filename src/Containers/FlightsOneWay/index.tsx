import React, { Component } from 'react'
import { View, FlatList, ActivityIndicator } from 'react-native'
import { OneWay } from '../../Components/OneWay'
import { findFlights } from '../../Services'
import Sugar from 'sugar'
import styles from './style'

export default class FlightsOneWay extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            oneWay: []
        }
    }
    componentDidMount() {
        const { dateFrom, cityFrom, cityTo } = this.props.data
        findFlights(cityFrom.aeroName, cityTo.aeroName, Sugar.Date.format(dateFrom, '{year}-{MM}-{dd}'))
            .then(flights => {
                this.setState({ oneWay: flights })
            })
    }
    render() {
        const { styBoxFlights } = styles
        const { oneWay } = this.state
        return (
            <View style={styBoxFlights}>
                {oneWay.length === 0 ? <ActivityIndicator size='large' color='rgba(255, 143, 41, 1.0)' /> :
                    <FlatList data={oneWay}
                        renderItem={({ item }) => <OneWay infoFlight={item} key={item._id} />}
                        keyExtractor={(item, index) => index.toString()} />}
            </View>
        )
    }
}