import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    styBoxFlights: {
        flex: 1,
        marginVertical: 10,
        paddingHorizontal: 10,
        width: 360
    }
})