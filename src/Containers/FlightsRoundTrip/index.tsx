import React, { Component } from 'react'
import { View, FlatList, ActivityIndicator } from 'react-native'
import { OneWay } from '../../Components/OneWay'
import { RoundTrip } from '../../Components/RoundTrip'
import { findRoundTrip } from '../../Services'
import Sugar from 'sugar'
import styles from './style'

export default class FlightsRoundTrip extends Component<{ Props }> {
    constructor(props) {
        super(props)
        this.state = {
            roundTripGroup: [],
            roundTripFrom: [],
            roundTripReturn: []
        }
    }
    componentDidMount() {
        const { dateFrom, dateTo, cityFrom, cityTo } = this.props.data
        findRoundTrip(cityFrom.aeroName, cityTo.aeroName, Sugar.Date.format(dateFrom, '{year}-{MM}-{dd}'),
            Sugar.Date.format(dateTo, '{year}-{MM}-{dd}'))
            .then(flightsRoundTrip => {
                this.setState({
                    roundTripGroup: flightsRoundTrip.group,
                    roundTripFrom: flightsRoundTrip.from,
                    roundTripReturn: flightsRoundTrip.return
                })
            })
    }
    render() {
        const { styBoxFlights } = styles
        const { roundTripGroup, roundTripFrom, roundTripReturn } = this.state

        return (
            <View style={styBoxFlights}>
                {roundTripGroup.length === 0 && roundTripFrom.length === 0 && roundTripReturn.length === 0 ?
                    <ActivityIndicator size='large' color='rgba(255, 143, 41, 1.0)' /> :
                    <View>
                        <FlatList data={roundTripGroup}
                            renderItem={({ item }) => <RoundTrip infoFlight={item} key={item._id} />}
                            keyExtractor={(item, index) => index.toString()} />
                        {roundTripFrom.length !== 0 && <FlatList data={roundTripFrom}
                            renderItem={({ item }) => <OneWay infoFlight={item} key={item._id} />}
                            keyExtractor={(item, index) => index.toString()} />}
                        {roundTripReturn.length !== 0 && <FlatList data={roundTripReturn}
                            renderItem={({ item }) => <OneWay infoFlight={item} key={item._id} />}
                            keyExtractor={(item, index) => index.toString()} />}
                    </View>
                }
            </View>
        )
    }
}