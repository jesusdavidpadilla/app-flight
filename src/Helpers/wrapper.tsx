import { Alert } from 'react-native'

export const validateForm = (arrayField) => {
    let valid = true
    arrayField.forEach( field => {
        if (!field) {
            valid = false
            return
        }
    })

    return valid
}

export const showAlert = (tittle, messaje) => {
    return Alert.alert(tittle, messaje)
}