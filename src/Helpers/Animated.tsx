import * as Animatable from 'react-native-animatable'

export const initializeButtonAnimate = () => {
    Animatable.initializeRegistryWithDefinitions({
        fill: {
            0: {
                opacity: 1
            },
            1: {
                opacity: 0
            }
        },
        waveOut: {
            0: {
                opacity: 1,
                scaleX: 0,
                scaleY: 0
            },
            1: {
                opacity: 0,
                scaleX: 5,
                scaleY: 5
            }
        }
    })
}