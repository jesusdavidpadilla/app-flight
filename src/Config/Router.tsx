import React from 'react'
import { Router, Scene, Lightbox } from 'react-native-router-flux'
import Home from '../Containers/Home/'
import FlightsOneWay from '../Containers/FlightsOneWay/'
import FlightsRoundTrip from '../Containers/FlightsRoundTrip/'
import { DetailFlight } from '../Components/DetailFlight'
import MultiFlights from '../Containers/MultiFlights/'

const sceneConfig = {
    cardStyle: {
        backgroundColor: 'rgba(248, 248, 248, 1.0)'
    }
}
export const Routes = () => (
  <Router>
    <Lightbox>
        <Scene key='root' {...sceneConfig}>
            <Scene key='home' component={Home} title='Flights'/>
            <Scene key='flightsOneWay' component={FlightsOneWay} title='Resultado de vuelos'/>
            <Scene key='flightsRoundTrip' component={FlightsRoundTrip} title='Resultado de vuelos'/>
            <Scene key='multiFlights' component={MultiFlights} title='Multidestino'/>
        </Scene>
        <Scene key='detailFlight' component={DetailFlight}/>
    </Lightbox>
  </Router>)