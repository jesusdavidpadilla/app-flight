# Flights

Proyecto en React Native como opción componente de la prueba final del semillero de Almundo - Medellín.
La aplicación permite consultar vuelos de ida, ida y regreso y multidestino según las consultas a una API REST desarrollada en express y mongoDB.


# Screenshots
![](https://preview.ibb.co/eSHkjH/Captura_de_pantalla_2018_04_26_a_la_s_11_38_44_a_m.png)


# Prerequisites

- Poseer el entorno de desarrollo Node
- Poseer en su máquina local el CLI de Reac-native.
- Watchman
- Android Studio o Genymotion
- Clonar el repositorio.

# Instalación

- Desde una terminal de la maquina, navegar entre carpetas hasta llegar a la del repositorio clonado.
- Ejecutar el comando:

> npm install

Luego de ejecutar el comando, se instalarán todas las dependencias del proyecto.

# Ejecución

Para ejecutar la aplicación puede ejecutar el siguiente comando:

> react-native run-android

## IMPORTANTE
Debe tener  la variables de entorno de android y el SDK del mismo

# Librerias usadas

- React - Librería JavaScript para construir interfaces de usuario.
- React Native - framework para construir apps nativas con React.
- react-native-router-flux: Usado para la navegación entre vistas.
- react-native-vector-icons - Pack Materialize
- react-native-orientation: Permite identificar los cambios de orienteación del dispositivo
- react-native-animatable: Permite agregar animaciones a componentes


# Versión

1.0.0


# Autores

Jesús David Padilla