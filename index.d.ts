/// <reference no-default-lib="true"/>
/// <reference path="node_modules/sugar/sugar.d.ts" />
/// <reference path="node_modules/sugar/sugar-extended.d.ts" />
interface ZeplinSize {
    height: number;
    width: number;
}